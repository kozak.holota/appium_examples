from time import sleep

from appium.webdriver.common.appiumby import AppiumBy
from appium.webdriver.extensions.android.nativekey import AndroidKey
from selenium.common.exceptions import NoSuchElementException

from pages.joom_abstract import JoomAbstract


class JoomMainPage(JoomAbstract):
    @property
    def initial_dialog_swiper(self):
        return self.driver.find_element(AppiumBy.ACCESSIBILITY_ID, "Close")

    @property
    def search_field_unfocused(self):
        return self.driver.find_element(AppiumBy.ANDROID_UIAUTOMATOR,
                                        'new UiSelector().resourceId("com.joom:id/search")')

    @property
    def search_field_focused(self):
        return self.driver.find_element(AppiumBy.ANDROID_UIAUTOMATOR,
                                        'new UiSelector().resourceId("com.joom:id/input")')
    @property
    def search_suggestions(self):
        return self.driver.find_elements(AppiumBy.XPATH, "//android.widget.Button")

    def close_initial_dialog(self):
        try:
            self.touch_action.tap(self.initial_dialog_swiper)
        except NoSuchElementException:
            pass
        return self

    def enter_search_item(self, item: str):
        self.touch_action.tap(self.search_field_unfocused).perform()
        self.touch_action.tap(self.search_field_unfocused).perform()
        # self.action.send_keys_to_element(self.search_field_unfocused, item).perform()
        self.driver.hide_keyboard()
        self.action.send_keys(item).perform()
        sleep(5)
        return self

    def select_search_suggestion(self, index):
        self.touch_action.tap(self.search_suggestions[index]).perform()
        return self

    def start_search(self):
        self.driver.press_keycode(AndroidKey.TAB).press_keycode(AndroidKey.SEARCH)

    def get_found_items(self):
        return self.driver.find_elements(AppiumBy.ANDROID_UIAUTOMATOR,
                                         'new UiSelector().resourceId("com.joom:id/product_view")')

    def get_item_prices(self):
        return self.driver.find_elements(AppiumBy.XPATH, '//*[contains(@content-desc, "UAH")]')
