from time import sleep

import pytest

from pages.joom_abstract import JoomAbstract


class TestSanity:
    @pytest.mark.first
    def test_first(self, main_page: JoomAbstract):
        main_page.driver.start_recording_screen()
        main_page.close_initial_dialog()
        main_page.enter_search_item("Barbie")
        main_page.select_search_suggestion(0)

        found_items_count = len(main_page.get_found_items())
        found_item_prices_count = len(main_page.get_item_prices())
        main_page.driver.stop_recording_screen()

        assert found_items_count > 1, "Found only one item! Very bad!"

        assert found_items_count == found_item_prices_count, f"Not every item for sell has it's price.\n " \
                                                             f"Items found: {found_items_count}.\n " \
                                                             f"Prices found: {found_item_prices_count}"

    @pytest.mark.shell_command
    def test_shell_commnd(self, main_page: JoomAbstract):
        ls_command = main_page.driver.execute_script("mobile:shell", {
            'command': 'ls',
            'args': ['-la'],
            'includeStderr': True,
            'timeout': 5000
        })

        main_page.driver.save_screenshot("joom_screen.png")

        assert not ls_command, f"Command returned a value:\n{ls_command}"
