from enum import Enum


class Capabilities(Enum):
    JOOM_ANDROID_CHROME = {
        "platformName": "Android",
        "automationName": "UiAutomator2",
        "platformVersion": "12",
        "browserName": "Chrome",
        "deviceName": "Test Nexus",
        "chromedriverExecutable": "/Users/kozak_mamay/bin/appium/chromedriver",
        "autoGrantPermissions": True
    }

    JOOM_ANDROID_APP = {
        "platformName": "Android",
        "automationName": "UiAutomator2",
        "platformVersion": "12",
        "deviceName": "Test Nexus",
        "app": "/Users/kozak_mamay/install/joom-4-1-0.apk"
    }
