from appium import webdriver

def get_browser(caps: dict) -> webdriver.Remote:
    return webdriver.Remote('http://localhost:4723/wd/hub', caps)
